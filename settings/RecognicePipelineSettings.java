package settings;

public class RecognicePipelineSettings implements PipelineSettings {

	private String inputFile;
	public String referenceGenome = "/mnt/SRV016/users/mft/genomes/Ecoli/W3110/STAR";
	public String referenceGenomeFile = "/mnt/SRV016/users/mft/genomes/Ecoli/W3110/Sequence/NC_007779.fa";
	public String referenceGTF = "/mnt/SRV016/users/mft/genomes/Ecoli/W3110/Annotation/UCSC_refSeq_ncRNA_sRNA_exonOnly_uniqSource.gtf";
	
	public String snpEffGenome = "Escherichia_coli_K_12_substr__W3110_uid161931";
	
	private String samplePrefix = "Default";
	private String outputFolder = "/mnt/SRV016/users/mft/tmp";
	
	private String projectPrefix = "Default";
	private String sampleFolderPrefix = "Sample_";
	
	public int numThreads = 4;
	
	public String forwardAdapter = "AGATCGGAAGAGCACACGTCTGAACTCCAGTCACGAGTTA";
	public String reverseAdapter = "AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTC";
	
	public void setSamplePrefix(String prefix) {
		this.samplePrefix = prefix;
	}
	
	public void setOutputFolder(String outFolder) {
		if(!outFolder.endsWith("/")) {
			outFolder += "/";
		}
		this.outputFolder = outFolder;
	}
	
	public void setInputFile(String input) {
		this.inputFile = input;
	}
	
	public String getInputFile() {
		return this.inputFile;
	}
	
	public String getOutputFolder() {
		return this.outputFolder;
	}
	
	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
	}

	@Override
	public String getSamplePrefix() {
		return samplePrefix;
	}
	
	public String getProjectPrefix() {
		return this.projectPrefix;
	}
	
	public void setProjectPrefix(String prefix) {
		this.projectPrefix = prefix;
	}
	
	public void setSampleFolderPrefix(String prefix) {
		this.sampleFolderPrefix = prefix;
	}
	
	public String getSampleFolderPrefix() {
		return this.sampleFolderPrefix;
	}
}
