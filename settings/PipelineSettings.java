package settings;

public interface PipelineSettings {
	
	public String getProjectPrefix();
	
	public String getSamplePrefix();
	
	public String getOutputFolder();
	
	public String getSampleFolderPrefix();

	public String getInputFile();
}
