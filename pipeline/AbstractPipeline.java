package pipeline;

import processors.Processor;
import processors.ProcessorQueue;
import settings.RecognicePipelineSettings;

public abstract class AbstractPipeline implements Pipeline {

	protected RecognicePipelineSettings settings;
	private ProcessorQueue queue;
	
	public AbstractPipeline(RecognicePipelineSettings settings) {
		this.settings = settings;
		queue = new ProcessorQueue(settings);
		this.constructPipeline();
	}
	
	public void add(Processor p) {
		this.queue.add(p);
	}
	
	public Processor getProcessor(int pIndex) {
		return this.queue.get(pIndex);
	}
	
	@Override
	public void execute() throws Exception {
		this.queue.runAll();
	}
	
	public abstract void constructPipeline();
}
