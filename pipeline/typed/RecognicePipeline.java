package pipeline.typed;

import pipeline.AbstractPipeline;
import processors.typed.external.FastQCProcessor;
import processors.typed.external.FreeBayesProcessor;
import processors.typed.external.HTSeqProcessor;
import processors.typed.external.PicardMarkDuplictateProcessor;
import processors.typed.external.SNPEffProcessor;
import processors.typed.external.STARProcessor;
import processors.typed.external.SamtoolsIndexProcessor;
import processors.typed.internal.VCFChromNameFixProcessor;
import processors.typed.internal.CleanUpProcessor;
import processors.typed.internal.FileRetriever;
import settings.PipelineSettings;
import settings.RecognicePipelineSettings;

public class RecognicePipeline extends AbstractPipeline {

	public RecognicePipeline(RecognicePipelineSettings settings) {
		super(settings);
	}

	@Override
	public void constructPipeline() {
		STARProcessor starProcessor = new STARProcessor(settings);
		PicardMarkDuplictateProcessor markDupProcessor = new PicardMarkDuplictateProcessor(settings);
		
		add(new FastQCProcessor(settings)); //fastqc quality check
		add(starProcessor); //mapping using STAR
		add(markDupProcessor); //mark duplicates in resulting bam file
		add(new SamtoolsIndexProcessor(settings)); //bam index of rmdup bam file
		add(new FileRetriever(settings, starProcessor)); //get result files from mapping
		add(new HTSeqProcessor(settings)); //run HTSeq on mapping files
		add(new FileRetriever(settings, markDupProcessor)); //get result files from duplicate removal
		add(new FreeBayesProcessor(settings)); //run variant calling
		add(new VCFChromNameFixProcessor(settings, "chr", "NC_007779")); //fix chr name to recognice name
		add(new SNPEffProcessor(settings));
		add(new CleanUpProcessor(settings)); //clean up unnecessary files and restructure output folder
	}

	@Override
	public PipelineSettings getSettings() {
		return settings;
	}
}
