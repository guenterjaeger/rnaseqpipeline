package pipeline;

import java.util.ArrayList;
import java.util.List;

public class PipelinePool {

	private List<Pipeline> pipelines;
	
	public PipelinePool() {
		this.pipelines = new ArrayList<Pipeline>();
	}
	
	public void addPipeline(Pipeline pipeline) {
		this.pipelines.add(pipeline);
	}
	
	public void execute() throws Exception {
		for(int i = 0; i < pipelines.size(); i++) {
			Pipeline p = pipelines.get(i);
			String samplePrefix = p.getSettings().getSamplePrefix();
			System.out.println("Executing Pipeline for sample " + samplePrefix);
			pipelines.get(i).execute();
		}
	}
}
