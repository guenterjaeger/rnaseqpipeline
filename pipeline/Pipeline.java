package pipeline;

import settings.PipelineSettings;

public interface Pipeline {

	public void execute() throws Exception;

	public PipelineSettings getSettings();
}
