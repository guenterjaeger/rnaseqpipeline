package processors;

import java.util.ArrayList;
import java.util.List;

import settings.RecognicePipelineSettings;

public class ProcessorQueue {
	List<Processor> processors;
	
	private RecognicePipelineSettings settings;
	
	public ProcessorQueue(RecognicePipelineSettings settings) {
		processors = new ArrayList<Processor>();
		this.settings = settings;
	}
	
	public void add(Processor p) {
		this.processors.add(p);
	}
	
	public Processor get(int index) {
		return this.processors.get(index);
	}
	
	public void runAll() throws Exception {
		String[] inFiles = {settings.getInputFile()};
		for(int i = 0; i < processors.size(); i++) {
			Processor p = processors.get(i);
			p.run(inFiles);
			inFiles = p.getOutFiles();
		}
	}
}
