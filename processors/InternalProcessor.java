package processors;

import settings.PipelineSettings;

public abstract class InternalProcessor implements Processor {

	protected PipelineSettings settings;
	protected String[] outFiles = new String[]{""};
	
	public InternalProcessor(PipelineSettings settings) {
		this.settings = settings;
	}

	@Override
	public String[] getOutFiles() {
		return this.outFiles;
	}
	
	@Override
	public void run(String... inFiles) throws Exception {
		this.setOutFiles();
		System.out.println();
		System.out.println("--------------------");
		System.out.println("Starting process " + getProcessName()); 
		System.out.println("--------------------");
		System.out.println();
		
		this.doWork(inFiles);
		
		System.out.println();
		System.out.println("--------------------");
		System.out.println("Finished " + getProcessName() + "!");
		System.out.println("--------------------");
		System.out.println();
	}
	
	public abstract void doWork(String... inFiles) throws Exception;
	
	public abstract void setOutFiles();
}
