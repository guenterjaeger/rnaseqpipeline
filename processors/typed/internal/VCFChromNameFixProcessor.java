package processors.typed.internal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

import processors.InternalProcessor;
import settings.RecognicePipelineSettings;

public class VCFChromNameFixProcessor extends InternalProcessor {

	String outfile;
	
	String oldChromName = "chr";
	String newChromName = "NC_007779";
	
	public VCFChromNameFixProcessor(RecognicePipelineSettings settings) {
		super(settings);
		this.outfile = settings.getOutputFolder()+settings.getSamplePrefix()+".chrFix.vcf";
	}

	public VCFChromNameFixProcessor(RecognicePipelineSettings settings, String oldName, String newName) {
		this(settings);
		this.oldChromName = oldName;
		this.newChromName = newName;
	}

	@Override
	public String getProcessName() {
		return "VCF Chromosome Name Fix";
	}

	@Override
	public void doWork(String... inFiles) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(inFiles[0]));
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		
		String line = null;
		
		int numFixed = 0;
		
		while((line = br.readLine()) != null) {
			if(line.startsWith("chr")) {
				line = line.replaceFirst(oldChromName, newChromName);
				numFixed++;
			}
			bw.write(line);
			bw.newLine();
		}
		
		br.close();
		bw.close();
		
		System.out.println("Number of fixed lines: " + numFixed);
	}

	@Override
	public void setOutFiles() {
		this.outFiles = new String[]{outfile};
	}

}
