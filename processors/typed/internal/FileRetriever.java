package processors.typed.internal;

import processors.ExternalProcessor;
import processors.InternalProcessor;
import settings.RecognicePipelineSettings;

public class FileRetriever extends InternalProcessor {

	private ExternalProcessor predecessor;
	
	public FileRetriever(RecognicePipelineSettings settings, ExternalProcessor predecessor) {
		super(settings);
		this.predecessor = predecessor;
	}

	@Override
	public void setOutFiles() {
		this.outFiles = predecessor.getOutFiles();
	}

	@Override
	public String getProcessName() {
		return "File Retriever from " + predecessor.getProcessName();
	}

	@Override
	public void doWork(String... inFiles) throws Exception {
		System.out.println("Files retrievied: ");
		
		String[] outFiles = getOutFiles();
		for(int i = 0; i < outFiles.length; i++) {
			System.out.println("\t" + outFiles[i]);
		}
	}
}
