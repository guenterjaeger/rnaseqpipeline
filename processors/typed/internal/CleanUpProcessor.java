package processors.typed.internal;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import processors.InternalProcessor;
import settings.RecognicePipelineSettings;

public class CleanUpProcessor extends InternalProcessor {

	public CleanUpProcessor(RecognicePipelineSettings settings) {
		super(settings);
	}

	@Override
	public void setOutFiles() {
		//nothing to do, will be done in the getCommand method
	}

	@Override
	public String getProcessName() {
		return "CleanUp";
	}

	@Override
	public void doWork(String... inFiles) throws Exception {
		File[] allFiles = new File(settings.getOutputFolder()).listFiles();
		
		File fastqcDir = new File(settings.getOutputFolder()+"fastqc");
		if(fastqcDir.exists() && fastqcDir.isDirectory()) { //delete existing fastqc files
			File[] fastqcFiles = fastqcDir.listFiles();
			for(File f : fastqcFiles) {
				f.delete();
			}
		} else {
			fastqcDir.mkdir(); //create a new directory if it does not exist
		}
		
		File vcDir = new File(settings.getOutputFolder()+"VariantCalling");
		if(vcDir.exists() && vcDir.isDirectory()) { //delete existing files
			File[] vcFiles = vcDir.listFiles();
			for(File f : vcFiles) {
				f.delete();
			}
		} else {
			vcDir.mkdir(); //create new variant calling directory if it does not exist
		}
		
		//
		for(int i = 0; i < allFiles.length; i++) {
			File f = allFiles[i];
			
			if(f.getAbsolutePath().endsWith(".out.junction")) {
				f.delete();
				System.out.println("Deleted: " + f.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().endsWith(".out.sam")) {
				f.delete();
				System.out.println("Deleted: " + f.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().endsWith("Log.out")) {
				f.delete();
				System.out.println("Deleted: " + f.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().endsWith("Log.progress.out")) {
				f.delete();
				System.out.println("Deleted: " + f.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().endsWith("out.tab")) {
				f.delete();
				System.out.println("Deleted: " + f.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().endsWith("metrics.log")) {
				f.delete();
				System.out.println("Deleted: " + f.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().endsWith(settings.getSamplePrefix()+".vcf")) {
				f.delete();
				System.out.println("Deleted: " + f.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().endsWith("chrFix.vcf")) {
				File fn = new File(vcDir.getAbsolutePath()+"/"+settings.getSamplePrefix()+".vcf");
				Files.move(f.toPath(), fn.toPath(), StandardCopyOption.ATOMIC_MOVE);
				System.out.println("  Moved: " + f.getAbsolutePath() + "\n     to: " + fn.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().endsWith("annotated.vcf")) {
				File fn = new File(vcDir.getAbsolutePath()+"/"+f.getName());
				Files.move(f.toPath(), fn.toPath(), StandardCopyOption.ATOMIC_MOVE);
				System.out.println("  Moved: " + f.getAbsolutePath() + "\n     to: " + fn.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().endsWith("snpEff_summary.genes.txt")) {
				File fn = new File(vcDir.getAbsolutePath()+"/"+f.getName());
				Files.move(f.toPath(), fn.toPath(), StandardCopyOption.ATOMIC_MOVE);
				System.out.println("  Moved: " + f.getAbsolutePath() + "\n     to: " + fn.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().endsWith("snpEff_summary.html")) {
				File fn = new File(vcDir.getAbsolutePath()+"/"+f.getName());
				Files.move(f.toPath(), fn.toPath(), StandardCopyOption.ATOMIC_MOVE);
				System.out.println("  Moved: " + f.getAbsolutePath() + "\n     to: " + fn.getAbsolutePath());
				continue;
			}
			
			if(f.getAbsolutePath().contains("_fastqc")) {
				
				File fn = new File(fastqcDir.getAbsolutePath()+"/"+f.getName());
				Files.move(f.toPath(), fn.toPath(), StandardCopyOption.ATOMIC_MOVE);
				System.out.println("  Moved: " + f.getAbsolutePath() + "\n     to: " + fn.getAbsolutePath());
				continue;
			}
		}
	}
}
