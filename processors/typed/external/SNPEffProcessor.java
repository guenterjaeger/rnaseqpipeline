package processors.typed.external;

import java.io.File;

import processors.ExternalProcessor;
import settings.RecognicePipelineSettings;
import main.Applications;

public class SNPEffProcessor extends ExternalProcessor {

	private String out;
	
	public SNPEffProcessor(RecognicePipelineSettings settings) {
		super(settings);
		out = settings.getOutputFolder() + settings.getSamplePrefix() + ".annotated.vcf";
		pb.redirectOutput(new File(out));
	}

	@Override
	public String getCommand(String... inFile) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(Applications.SNPEFF);
		sb.append(" ");
		sb.append("eff");
		sb.append(" ");
		sb.append("-ud 1000"); //elongate up and downstream reagions
		sb.append(" ");
		sb.append("-o gatk"); //output in gatk format
		sb.append(" ");
		sb.append("-s");
		sb.append(" ");
		sb.append(settings.getOutputFolder()+settings.getSamplePrefix()+".snpEff_summary.html");
		sb.append(" ");
		sb.append(settings.snpEffGenome);
		sb.append(" ");
		sb.append(inFile[0]);
		
		return sb.toString();
	}

	@Override
	public void setOutFiles() {
		this.outFiles =  new String[]{out};
	}

	@Override
	public String getProcessName() {
		return "SNPEff";
	}
}
