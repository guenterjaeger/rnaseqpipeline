package processors.typed.external;

import processors.ExternalProcessor;
import settings.RecognicePipelineSettings;
import main.Applications;

public class FastQCProcessor extends ExternalProcessor {

	public FastQCProcessor(RecognicePipelineSettings settings) {
		super(settings);
	}

	@Override
	public String getCommand(String... inFile) {
		this.outFiles = inFile; // just pass on the files
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(Applications.FASTQC);
		
		if(inFile.length > 0) {
			sb.append(" ");
			sb.append(inFile[0]);
		}
		
		for(int i = 1; i < inFile.length; i++) {
			sb.append(" ");
			sb.append(inFile[i]);
		}
		
		return sb.toString();
	}

	@Override
	public void setOutFiles() {
		//already set in the getCommand method
	}

	@Override
	public String getProcessName() {
		return "FASTQC";
	}
}
