package processors.typed.external;

import processors.ExternalProcessor;
import settings.RecognicePipelineSettings;
import main.Applications;

public class SamtoolsIndexProcessor extends ExternalProcessor {

	public SamtoolsIndexProcessor(RecognicePipelineSettings settings) {
		super(settings);
	}

	@Override
	public String getCommand(String... inFile) {
		this.outFiles = inFile;
		StringBuilder sb = new StringBuilder();
		sb.append(Applications.SAMTOOLS);
		sb.append(" ");
		sb.append("index");
		sb.append(" ");
		sb.append(inFile[0]); //currently only the first bam file can be indexed
		return sb.toString();
	}

	@Override
	public void setOutFiles() {
		//is already set in the getCommand method
	}

	@Override
	public String getProcessName() {
		return "samtools index";
	}
}
