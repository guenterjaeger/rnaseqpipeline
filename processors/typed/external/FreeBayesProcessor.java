package processors.typed.external;

import java.io.File;

import main.Applications;
import processors.ExternalProcessor;
import settings.RecognicePipelineSettings;

public class FreeBayesProcessor extends ExternalProcessor {

	private String outfile;
	
	public FreeBayesProcessor(RecognicePipelineSettings settings) {
		super(settings);
		this.outfile = settings.getOutputFolder() + settings.getSamplePrefix() + ".vcf";
		pb.redirectOutput(new File(outfile));
	}

	@Override
	public String getCommand(String... inFile) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(Applications.FREEBAYES);
		sb.append(" ");
		sb.append("-f " + settings.referenceGenomeFile);
		sb.append(" ");
		sb.append("--min-coverage 3");
		sb.append(" ");
		sb.append("--min-mapping-quality 2");
		sb.append(" ");
		sb.append("--min-alternate-fraction 0.80");
		sb.append(" ");
		sb.append("--min-base-quality 20");
		sb.append(" ");
		sb.append("-b " + inFile[0]);
		sb.append(" ");
		sb.append("-p 1");
		
		return sb.toString();
	}

	@Override
	public void setOutFiles() {
		this.outFiles = new String[]{outfile};
	}

	@Override
	public String getProcessName() {
		return "FreeBayes";
	}
}
