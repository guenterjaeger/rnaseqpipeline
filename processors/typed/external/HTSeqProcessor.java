package processors.typed.external;

import java.io.File;

import processors.ExternalProcessor;
import settings.RecognicePipelineSettings;
import main.Applications;

public class HTSeqProcessor extends ExternalProcessor {
	
	private String outputFile;
	
	public HTSeqProcessor(RecognicePipelineSettings settings) {
		super(settings);
		outputFile = settings.getOutputFolder() + settings.getSamplePrefix() + ".HTSeq.tsv";
		pb.redirectOutput(new File(outputFile)); //write results to the output file
	}

	@Override
	public String getCommand(String... inFile) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(Applications.HTSEQ);
		sb.append(" ");
		sb.append("-f bam"); //input in bam format
		sb.append(" ");
		sb.append("-m intersection-nonempty"); //use htseq-count in the intersection non-empty mode
		sb.append(" ");
		sb.append("-s no");
		sb.append(" ");
		sb.append("-a 3"); //set min base quality to 3
		sb.append(" ");
		sb.append("-t exon");
		sb.append(" ");
		sb.append("-i gene_id");
		sb.append(" ");
		sb.append(inFile[0]);
		sb.append(" ");
		sb.append(settings.referenceGTF);
		//output writing has moved to the constructor
		
		return sb.toString();
	}

	@Override
	public void setOutFiles() {
		this.outFiles = new String[]{outputFile};
	}

	@Override
	public String getProcessName() {
		return "htseq-count";
	}
}
