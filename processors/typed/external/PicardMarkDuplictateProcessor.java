package processors.typed.external;

import processors.ExternalProcessor;
import settings.RecognicePipelineSettings;
import main.Applications;

public class PicardMarkDuplictateProcessor extends ExternalProcessor {

	private String outfile = "";
	private String metrics = "";
	
	public PicardMarkDuplictateProcessor(RecognicePipelineSettings settings) {
		super(settings);
		this.outfile = settings.getOutputFolder() + settings.getSamplePrefix() + ".rmdup.bam";
		this.metrics = settings.getOutputFolder() + settings.getSamplePrefix() + ".rmdup.metrics.log";
	}

	@Override
	public String getCommand(String... inFile) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(Applications.PICARD_MARK_DUPLICATES);
		sb.append(" ");
		sb.append("I=" + inFile[0]); //infile
		sb.append(" ");
		sb.append("O=" + outfile); //outfile
		sb.append(" ");
		sb.append("M=" + metrics); //metrics file
		sb.append(" ");
		sb.append("AS=true");
		
		return sb.toString();
	}

	@Override
	public void setOutFiles() {
		this.outFiles = new String[]{outfile, metrics};
	}

	@Override
	public String getProcessName() {
		return "Picard MarkDuplicates";
	}
}
