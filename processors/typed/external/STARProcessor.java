package processors.typed.external;

import processors.ExternalProcessor;
import settings.RecognicePipelineSettings;
import main.Applications;

public class STARProcessor extends ExternalProcessor {
	
	public STARProcessor(RecognicePipelineSettings settings) {
		super(settings);
	}

	@Override
	public String getCommand(String ... inFiles) {
		StringBuilder sb = new StringBuilder();
		sb.append(Applications.STAR);
		sb.append(" ");
		sb.append("--genomeDir " + settings.referenceGenome); //set the reference genome folder
		sb.append(" ");
		sb.append("--readFilesIn " + inFiles[0]); //set forwards reads input file
		sb.append(" ");
		if(inFiles.length > 1) { //set reverse reads input file if available
			sb.append(inFiles[1]);
			sb.append(" ");
		}
		sb.append("--runThreadN " + settings.numThreads); //set number of threads
		sb.append(" ");
		sb.append("--clip3pAdapterSeq " + settings.forwardAdapter + " " + settings.reverseAdapter); //set adapter sequences
		sb.append(" ");
		sb.append("--outFileNamePrefix " + settings.getOutputFolder() + "/" + settings.getSamplePrefix()); //prefix for the output file name
		sb.append(" ");
		sb.append("--clip3pNbases 0"); //clip bases from the 3p end
		sb.append(" ");
		sb.append("--clip5pNbases 0"); //clip bases from the 5p end
		sb.append(" ");
		sb.append("--outSAMstrandField intronMotif");
		sb.append(" ");
		
		if(inFiles[0].endsWith(".gz")) {
			sb.append("--readFilesCommand zcat"); //if files are in gz format then use zcat to read them
			sb.append(" ");
		}
		
		sb.append("--chimSegmentMin 50");
		sb.append(" ");
		sb.append("--chimScoreMin 1");
		sb.append(" ");
		sb.append("--seedSearchStartLmax 50");
		sb.append(" ");
		sb.append("--alignIntronMin 2"); // prevent splicing
		sb.append(" ");
		sb.append("--alignIntronMax 1"); // prevent splicing
		sb.append(" ");
		sb.append("--outSAMtype BAM SortedByCoordinate");
		sb.append(" ");
		sb.append("--limitBAMsortRAM 21474836480"); //give at least 20GB of RAM for BAM file sorting
		sb.append(" ");
		sb.append("--outSAMattrRGline ID:1 PL:illumina PU:RGPU LB:N SM:RGSM CN:MFT"); //add read groups

		return sb.toString();
	}

	@Override
	public void setOutFiles() {
		String outFile = settings.getOutputFolder()+settings.getSamplePrefix()+"Aligned.sortedByCoord.out.bam";
		this.outFiles = new String[]{outFile};
	}

	@Override
	public String getProcessName() {
		return "STAR";
	}
}
