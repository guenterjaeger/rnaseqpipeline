package processors.typed.multisample;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import processors.InternalProcessor;
import settings.PipelineSettings;

public class HTSeqExpressionTableProcessor extends InternalProcessor {
	
	private Map<String, List<Integer>> expTable;
	private ArrayList<String> rowHeader;
	
	public HTSeqExpressionTableProcessor(PipelineSettings settings) {
		super(settings);
		this.expTable = new TreeMap<String, List<Integer>>();
		this.rowHeader = new ArrayList<String>();
	}
	
	public void writeTable(File outFile) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
		
		//row header
		bw.write("Gene");
		for(String colName : expTable.keySet()) {
			bw.write("\t");
			bw.write(colName);
		}
		bw.newLine();
		
		//values
		
		for(int i = 0; i < rowHeader.size(); i++) {
			bw.write(rowHeader.get(i));
			for(String colName : expTable.keySet()) {
				bw.write("\t");
				bw.write(Integer.toString(expTable.get(colName).get(i)));
			}
			bw.newLine();
		}
		
		bw.flush();
		bw.close();
	}

	public void addFile(String prefix, File file) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = null;
		boolean withHeader = false;
		
		
		if(this.expTable.isEmpty()) {
			withHeader = true;
		}
		
		if(!this.expTable.containsKey(prefix)) {
			this.expTable.put(prefix, new ArrayList<Integer>());
		}
		
		while((line = br.readLine()) != null) {
			String[] split = line.split("\t");
			
			if(!split[0].startsWith("_")) {
				if(withHeader) {
					rowHeader.add(split[0]);
				}
				int value = Integer.parseInt(split[1]);
				this.expTable.get(prefix).add(value);
			}
		}
		
		br.close();
	}

	@Override
	public String getProcessName() {
		return "HTSeq Expression Table Generator";
	}

	@Override
	public void doWork(String... inFiles) throws Exception {
		File htSeqDir = new File(settings.getOutputFolder()+"HTSeq");
		
		if(htSeqDir.exists() && htSeqDir.isDirectory()) {
			File[] files = htSeqDir.listFiles();
			for(File f : files) {
				f.delete();
			}
		} else {
			htSeqDir.mkdir();
		}
		
		File inputFolder = new File(inFiles[0]);
		File[] sampleFolders = inputFolder.listFiles();
		
		int filesFound = 0;
		int numSamples = 0;
		
		for(int i = 0; i < sampleFolders.length; i++) {
			File sampleFolder = sampleFolders[i];
			if(sampleFolder.isDirectory() && sampleFolder.getName().startsWith(settings.getSampleFolderPrefix())) {
				numSamples++;
				File[] htFiles = sampleFolder.listFiles();
				for(int j = 0; j < htFiles.length; j++) {
					if(htFiles[j].getName().endsWith(".HTSeq.tsv")) {
						String prefix = htFiles[j].getName().replace(".HTSeq.tsv", "");
						addFile(prefix, htFiles[j]);
						filesFound++;
						break;
					}
				}
			}
		}
		
		System.out.println(numSamples + " samples found");
		System.out.println(filesFound + " htseq files found");
			
		writeTable(new File(getOutFiles()[0]));
	}

	@Override
	public void setOutFiles() {
		this.outFiles = new String[]{settings.getOutputFolder()+"HTSeq/"+settings.getProjectPrefix()+".expressionTable.raw.tsv"};
	}
}
