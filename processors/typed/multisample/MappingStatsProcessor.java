package processors.typed.multisample;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import processors.InternalProcessor;
import settings.PipelineSettings;

public class MappingStatsProcessor extends InternalProcessor {

	public static final int numFields = 4;
	private Map<String, int[]> table;
	
	public MappingStatsProcessor(PipelineSettings settings) {
		super(settings);
		table = new TreeMap<String, int[]>();
	}

	@Override
	public String getProcessName() {
		return "Mapping Statistics Calculation";
	}

	@Override
	public void doWork(String... inFiles) throws Exception {
		
		File inputPath = new File(settings.getInputFile());
		String folderPrefix = settings.getSampleFolderPrefix();
		File outFile = new File(getOutFiles()[0]);
		
		if(!inputPath.isDirectory()) {
			System.err.println("ERROR: " + inputPath.getAbsolutePath() + " has to be a directory!");
		}
		
		File[] files = inputPath.listFiles();
		for(int i = 0; i < files.length; i++) {
			if(files[i].isDirectory()) {
				if(files[i].getName().startsWith(folderPrefix)) {
					File[] folderFiles = files[i].listFiles();
					
					for(int j = 0; j < folderFiles.length; j++) {
						if(folderFiles[j].getName().endsWith("Log.final.out")) { //search for the STAR output file
							String prefix = folderFiles[j].getName().replace("Log.final.out", "");
							addSTARFile(prefix, folderFiles[j]);
							System.out.println("Added STAR file " + folderFiles[j].getName());
						}
						if(folderFiles[j].getName().endsWith("HTSeq.tsv")) {
							String prefix = folderFiles[j].getName().replace(".HTSeq.tsv", "");
							addHTSeqFile(prefix, folderFiles[j]);
							System.out.println("Added HTSeq file " + folderFiles[j].getName());
						}
					}
				}
			}
		}
		
		write(outFile);
	}

	@Override
	public void setOutFiles() {
		String outFile = settings.getOutputFolder()+settings.getProjectPrefix()+".MappingStats.tsv";
		this.outFiles = new String[]{outFile};
	}
	
	private void write(File outFile) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
		
		//write header
		bw.write("Sample");
		bw.write("\t");
		bw.write("Available Reads");
		bw.write("\t");
		bw.write("Mapped Reads");
		bw.write("\t");
		bw.write("% Mapped Reads");
		bw.write("\t");
		bw.write("Uniquely Mapped Reads");
		bw.write("\t");
		bw.write("% Uniquely Mapped Reads");
		bw.write("\t");
		bw.write("Reads Mapped To Multiple Loci");
		bw.write("\t");
		bw.write("% Reads Mapped To Multiple Loci");
		bw.write("\t");
		bw.write("Reads Mapped To Genes");
		bw.write("\t");
		bw.write("% Reads Mapped To Genes");
		bw.newLine();
		
		//write entries
		for(String prefix : table.keySet()) {
			int[] entry = table.get(prefix);
			bw.write(prefix);
			bw.write("\t");
			bw.write(Integer.toString(entry[0])); //available reads
			bw.write("\t");
			bw.write(Integer.toString(entry[1]+entry[2])); //mapped reads
			bw.write("\t");
			double perc_mapped = Math.round((double)(entry[1]+entry[2])/(double)entry[0]*10000.)/100.; 
			bw.write(Double.toString(perc_mapped).replaceAll("\\.", ",")); //% uniquely mapped reads
			bw.write("\t");
			bw.write(Integer.toString(entry[1])); //uniquely mapped reads
			bw.write("\t");
			double perc_uniq_mapped = Math.round((double)entry[1]/(double)entry[0]*10000.)/100.;
			bw.write(Double.toString(perc_uniq_mapped).replaceAll("\\.", ","));
			bw.write("\t");
			bw.write(Integer.toString(entry[2])); //multiple loci reads
			bw.write("\t");
			double perc_multi_mapped = Math.round((double)entry[2]/(double)entry[0]*10000.)/100.;
			bw.write(Double.toString(perc_multi_mapped).replaceAll("\\.", ",")); //% mutiple loci reads
			bw.write("\t");
			bw.write(Integer.toString(entry[3])); //reads to genes
			bw.write("\t");
			double perc_genes = Math.round((double)entry[3]/(double)entry[0]*10000.)/100.;
			bw.write(Double.toString(perc_genes).replaceAll("\\.", ",")); //% reads to genes
			bw.newLine();
		}
		
		bw.flush();
		bw.close();
	}

	private void addSTARFile(String prefix, File file) throws IOException {
		this.checkContains(prefix);
		int[] entry = table.get(prefix);
		
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = null;
		
		while((line = br.readLine()) != null) {
			line = line.trim();
			
			if(line.startsWith("Number of input reads")) {
				String[] split = line.split("\\|");
				String number = split[1].trim();
				entry[0] = Integer.parseInt(number);
			}
			
			if(line.startsWith("Uniquely mapped reads number")) {
				String[] split = line.split("\\|");
				String number = split[1].trim();
				entry[1] = Integer.parseInt(number);
			}
			
			if(line.startsWith("Number of reads mapped to multiple loci")) {
				String[] split = line.split("\\|");
				String number = split[1].trim();
				entry[2] = Integer.parseInt(number);
			}
		}
		
		br.close();
	}

	private void addHTSeqFile(String prefix, File file) throws IOException {
		this.checkContains(prefix);
		int[] entry = table.get(prefix);
		
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = null;
		
		int sum = 0;
		
		while((line = br.readLine()) != null) {
			String[] split = line.split("\t");
			
			if(!line.startsWith("_")) {
				String number = split[1].trim();
				sum += Integer.parseInt(number);
			}
		}
		
		br.close();
		
		entry[3] = sum;
	}
	
	private void checkContains(String prefix) {
		if(!table.containsKey(prefix)) {
			table.put(prefix, new int[numFields]);
		}
	}
}
