package processors;

public interface Processor {
	
	public void run(String... inFiles) throws Exception;
	
	public String[] getOutFiles();
	
	public String getProcessName();
	
}
