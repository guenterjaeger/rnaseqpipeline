package processors;

import java.lang.ProcessBuilder.Redirect;
import java.util.List;

import main.StringTools;
import settings.RecognicePipelineSettings;

public abstract class ExternalProcessor implements Processor {

	protected ProcessBuilder pb = new ProcessBuilder();
	protected RecognicePipelineSettings settings;
	
	protected String[] outFiles = new String[]{""};
	
	public ExternalProcessor(RecognicePipelineSettings settings) {
		this.settings = settings;
		pb = new ProcessBuilder();
		pb.redirectError(Redirect.INHERIT);
	}
	
	public void run(String... inFiles) throws Exception {
		this.setOutFiles();
		System.out.println();
		System.out.println("--------------------");
		System.out.println("Starting process " + getProcessName() + " with command:");
		System.out.println();
		
		String command = getCommand(inFiles);
		pb.command(asArray(command));
		
		System.out.println(StringTools.formatCommandLine(commandLine(pb.command())));
		
		System.out.println("--------------------");
		System.out.println();
		
		Process p = this.pb.start();
		p.waitFor();
		
		System.out.println();
		System.out.println("--------------------");
		System.out.println("Finished " + getProcessName() + "!");
		System.out.println("--------------------");
		System.out.println();
	}
	
	private String[] asArray(String command) {
		return command.split(" ");
	}

	private String commandLine(List<String> command) {
		StringBuilder sb = new StringBuilder();
		for(String s : command) {
			sb.append(s);
			sb.append(" ");
		}
		return sb.toString();
	}
	
	public abstract String getCommand(String ... inFile);
	
	public String[] getOutFiles() {
		return this.outFiles;
	}
	
	public abstract void setOutFiles();
	
	public abstract String getProcessName();
}
