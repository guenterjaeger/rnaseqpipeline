package main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.OptionHandlerFilter;

import pipeline.PipelinePool;
import pipeline.typed.RecognicePipeline;
import processors.ProcessorQueue;
import processors.typed.multisample.HTSeqExpressionTableProcessor;
import processors.typed.multisample.MappingStatsProcessor;
import settings.RecognicePipelineSettings;

public class MultiPipelineStarter {

	@Option(name="-x", usage="Processing Pipeline", metaVar="STRING", required=true,aliases={"--processing-pipeline"})
	private String processingPipeline;
	
	@Option(name="-f", usage="Project Folder", metaVar="FOLDER", required=true, aliases={"--input-folder"})
	private String projectFolder;
	
	@Option(name="-p", usage="Sample Folder Prefix", metaVar="STRING", required=true, aliases={"--sample-folder-prefix"})
	private String sampleFolderPrefix;
	
	@Argument
	private List<String> arguments = new ArrayList<String>();
	
	public boolean doMain(String[] args) throws Exception {
		CmdLineParser parser = new CmdLineParser(this);
		parser.getProperties().withUsageWidth(120);
		
		try {
			parser.parseArgument(args);
		} catch (CmdLineException ex) {
			System.err.println(ex.getMessage());
			System.err.println("java -jar PipelineStarter.jar -multi [options...] arguments...");
			parser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java -jar PipelineStarter.jar -multi"+parser.printExample(OptionHandlerFilter.ALL));
			return false;
		}
		
		File projectDir = new File(projectFolder);
		File[] sampleFolders = projectDir.listFiles();
		
		PipelinePool pool = new PipelinePool();
		
		List<File> sampleDirs = new ArrayList<File>();
		
		for(int i = 0; i < sampleFolders.length; i++) {
			File f = sampleFolders[i];
			if(f.isDirectory()) {
				if(f.getName().startsWith(sampleFolderPrefix)) {
					String samplePrefix = f.getName().substring(sampleFolderPrefix.length());
					String sampleOutputFolder = f.getAbsolutePath();
					String inputFile = null;
					
					sampleDirs.add(f);
					
					File[] fastqFiles = f.listFiles();
					for(int j = 0; j < fastqFiles.length; j++) {
						File fq = fastqFiles[j];
						if(fq.getName().endsWith("fastq") 
								|| fq.getName().endsWith("fq") 
								|| fq.getName().endsWith("fastq.gz") 
								|| fq.getName().endsWith("fq.gz")) {
							inputFile = fq.getAbsolutePath();
							break;
						}
					}
					
					if(processingPipeline.equals("Recognice")) {
						RecognicePipelineSettings settings = new RecognicePipelineSettings();
						
						settings.setSamplePrefix(samplePrefix);
						settings.setOutputFolder(sampleOutputFolder);
						settings.setInputFile(inputFile);
						
						RecognicePipeline pipeline = new RecognicePipeline(settings);
						pool.addPipeline(pipeline);
					}
					
					//TODO add more pipelines here
				}
			}
		}
		
		//start pipeline consecutively
		pool.execute();
		
		//perform operations needed for all samples in parallel
		if(processingPipeline.equals("Recognice")) {
			RecognicePipelineSettings settings = new RecognicePipelineSettings();
			
			String projectPath = projectDir.getAbsolutePath();
			if(projectPath.endsWith("/.")) {
				projectPath = projectPath.substring(0, projectPath.length()-2);
			}
			
			String[] projectPathSplit = projectPath.split("/");
			String projectPrefix = projectPathSplit[projectPathSplit.length-1].split("_")[0];
			
			settings.setSampleFolderPrefix(this.sampleFolderPrefix);
			settings.setProjectPrefix(projectPrefix);
			settings.setOutputFolder(projectFolder);
			settings.setInputFile(projectFolder);
			
			ProcessorQueue pq = new ProcessorQueue(settings);
			
			HTSeqExpressionTableProcessor htSeqExpTableProessor = new HTSeqExpressionTableProcessor(settings);
			//TODO expression table normalization?
			MappingStatsProcessor mappingStatsProcessor = new MappingStatsProcessor(settings);
			
			pq.add(htSeqExpTableProessor);
			pq.add(mappingStatsProcessor);
			
			pq.runAll();
		}
		
		return true;
	}
}
