package main;

public class Applications {

	public static final String STAR = "/mnt/share/opt/STAR-2.4.0i/bin/Linux_x86_64/STAR";
	public static final String SAMTOOLS = "/mnt/share/opt/samtools-1.1/bin/samtools";
	public static final String TOPAS = "java -jar /mnt/SRV016/users/mft/software/topas.jar JoinExprTables";
	public static final String FASTQC = "/mnt/share/opt/fastqc-0.11.2/fastqc";
	public static final String HTSEQ = "/usr/local/bin/htseq-count";
	public static final String FREEBAYES = "/mnt/share/opt/freebayes20150204/bin/freebayes";
	public static final String SNPEFF = "java -jar /mnt/SRV016/users/mft/software/snpEff/snpEff.jar";
	public static final String PICARD_MARK_DUPLICATES = "java -Xmx10g -jar /mnt/share/opt/picard-tools-1.122/MarkDuplicates.jar";
	
}
