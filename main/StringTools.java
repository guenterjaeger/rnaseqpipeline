package main;

public class StringTools {

	public static String formatCommandLine(String commandLine) {
		StringBuilder sb = new StringBuilder();
		
		String[] split = commandLine.split(" ");
		
		for(int i = 0; i < split.length; i++) {
			if(split[i].startsWith("-")) {
				if(split.length > i+1) {
					if(!split[i+1].startsWith("-")) {
						sb.append(split[i] + " " + split[i+1]);
						sb.append("\n\t");
						i++;
						continue;
					}
				}
			}
			
			sb.append(split[i]);
			sb.append("\n\t");
		}
		
		return sb.toString();
	}
}
