package main;

import java.util.ArrayList;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.OptionHandlerFilter;

public class PipelineStarter {

	@Option(name="-multi",usage="Run multiple pipeline instances consecutively",required=false,aliases={"--multiple-pipelines"}, forbids="-single")
	private boolean multi = false;
	
	@Option(name="-single",usage="Run a single pipeline instance", required=false, aliases={"--single-pipeline"}, forbids={"-multi"})
	private boolean single = false;
	
	@Argument
	private List<String> arguments = new ArrayList<String>();
	
	@SuppressWarnings("deprecation")
	public void doMain(String[] args) throws Exception {
		CmdLineParser parser = new CmdLineParser(this);
		parser.getProperties().withUsageWidth(120);
		
		try {
			if(args.length > 0) {
				String[] mode = new String[]{args[0]};
				parser.parseArgument(mode);
			} else {
				throw new CmdLineException("Either -single or -multi is required!");
			}
			
			if(!multi && !single) {
				throw new CmdLineException("Either -single or -multi is required!");
			}
			
		} catch (CmdLineException ex) {
			System.err.println(ex.getMessage());
			System.err.println("java -jar PipelineStarter.jar [options...] arguments...");
			parser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java -jar PipelineStarter.jar" + parser.printExample(OptionHandlerFilter.ALL));
			return;
		}
		
		String[] subArgs = getSubArgs(args);
		boolean success = false;
		
		if(multi) {
			MultiPipelineStarter multiStarter = new MultiPipelineStarter();
			success = multiStarter.doMain(subArgs);
		} else if(single){
			SinglePipelineStarter singleStarter = new SinglePipelineStarter();
			success = singleStarter.doMain(subArgs);
		}
		
		if(success) {
			System.out.println("All done!");
		}
	}
	
	private String[] getSubArgs(String[] args) {
		String[] subArgs = new String[args.length - 1];
		for(int i = 1; i < args.length; i++) {
			subArgs[i-1] = args[i];
		}
		return subArgs;
	}
	
	public static void main(String[] args) throws Exception {
		new PipelineStarter().doMain(args);
	}
}
