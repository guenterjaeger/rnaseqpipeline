package main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.OptionHandlerFilter;

import pipeline.Pipeline;
import pipeline.typed.RecognicePipeline;
import settings.RecognicePipelineSettings;

public class SinglePipelineStarter {

	@Option(name="-x", usage="Processing Pipeline", metaVar="STRING", required=true,aliases={"--processing-pipeline"})
	private String processingPipeline;
	
	@Option(name="-o", usage="Sample Folder", metaVar="FOLDER", required=true, aliases={"--sample-folder"})
	private String sampleFolder;
	
	@Option(name="-p", usage="Sample Prefix", metaVar="STRING", required=true, aliases={"--sample-prefix"})
	private String samplePrefix;
	
	@Argument
	private List<String> arguments = new ArrayList<String>();
	
	public boolean doMain(String[] args) throws Exception {
		CmdLineParser parser = new CmdLineParser(this);
		parser.getProperties().withUsageWidth(120);
		
		try {
			parser.parseArgument(args);
		} catch (CmdLineException ex) {
			System.err.println(ex.getMessage());
			System.err.println("java -jar PipelineStarter.jar -multi [options...] arguments...");
			parser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java -jar PipelineStarter.jar -multi"+parser.printExample(OptionHandlerFilter.ALL));
			return false;
		}
		
		String inputFile = null;
		
		File[] fastqFiles = new File(sampleFolder).listFiles();
		for(int j = 0; j < fastqFiles.length; j++) {
			File fq = fastqFiles[j];
			if(fq.getName().endsWith("fastq") 
					|| fq.getName().endsWith("fq") 
					|| fq.getName().endsWith("fastq.gz") 
					|| fq.getName().endsWith("fq.gz")) {
				inputFile = fq.getAbsolutePath();
				break;
			}
		}
		
		if(processingPipeline.equals("Recognice")) {
			RecognicePipelineSettings settings = new RecognicePipelineSettings();
			
			settings.setSamplePrefix(samplePrefix);
			settings.setOutputFolder(sampleFolder);
			settings.setInputFile(inputFile);
			
			Pipeline pipeline = new RecognicePipeline(settings);
			pipeline.execute();
		}
		
		//TODO add other pipelines here
		
		return true;
	}
}
